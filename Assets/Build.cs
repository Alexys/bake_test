// C# example
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class Build
{
    public static void PerformBuild()
    {
        EditorSceneManager.OpenScene("Assets/_Scene/S_test-light.unity");
        Lightmapping.Clear();
        Lightmapping.ClearDiskCache();
        Lightmapping.ClearLightingDataAsset();
        Lightmapping.Bake();
    }
}
